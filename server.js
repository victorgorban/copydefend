//Filename: express_bootstrap.js
const express = require('express');
const bodyParser = require('body-parser')
const formidableMiddleware = require('express-formidable');

//NPM Module to integrate Handlerbars UI template engine with Express
const exphbs = require('express-handlebars');
const handleFormRequest = require('./handleFormRequest');

var app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(formidableMiddleware());

//Declaring Express to use Handlerbars template engine with main.handlebars as
//the default layout
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

//Defining middleware to serve static files
app.use(express.static(__dirname + '/public'));

// app.get("/home", function (req, res) {
//     res.render("home")
// });

app.get("/", function (req, res) {
    res.render("index")
});

handleFormRequest(app);

app.listen(3007, function () {
    console.log(`Server up: http://localhost:${3007}`);
});