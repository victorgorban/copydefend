const { AlignmentType, BorderStyle } = require("docx");

let noBorders = {
  bottom: {
    style: BorderStyle.DOUBLE,
    size: 2,
    color: "white",
  },
  top: {
    style: BorderStyle.DOUBLE,
    size: 2,
    color: "white",
  },
  left: {
    style: BorderStyle.DOUBLE,
    size: 2,
    color: "white",
  },
  right: {
    style: BorderStyle.DOUBLE,
    size: 2,
    color: "white",
  },
};

function docInfo(docTitle = "", options = {}) {
  return {
    title: docTitle,
    styles: {
      paragraphStyles: [
        {
          id: "Heading1",
          name: "Heading 1",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            size: 32,
            bold: true,
            italics: true,
          },
          paragraph: {
            alignment: AlignmentType.CENTER,
            spacing: {
              // before: 240,
              // after: 120,
            },
          },
        },
        {
          id: "Heading1JustBold",
          name: "Heading1, but just bold and 14pt. No italic, no centering, no color",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Verdana",
            size: 28,
            bold: true
          },
          paragraph: {
            alignment: AlignmentType.START,
            spacing: {
              // before: 240,
              // after: 120,
            },
          },
        },
        {
          id: "Default14",
          name: "Default 14pt",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 24, // при открытии размер на 2 делится почему-то
            // bold: true,
          },
        },
        {
          id: "Bold14",
          name: "Bold 14pt",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 28, // при открытии размер на 2 делится почему-то
            bold: true,
          },
        },
        {
          id: "TableCellDefault",
          name: "Table cell default 10pt",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Helvetica Neue",
            size: 20, // при открытии размер на 2 делится почему-то
            // bold: true,
          },
          paragraph: {
            alignment: AlignmentType.RIGHT,
          },
        },
        {
          id: "TableCellHeader",
          name: "Table cell header 10pt",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Helvetica Neue",
            size: 20, // при открытии размер на 2 делится почему-то
            bold: true,
          },
          paragraph: {
            alignment: AlignmentType.LEFT,
          },
        },
      ],
    },
    ...options
  };
}

module.exports = {
  noBorders,
  docInfo,
};
