const uniqid = require('uniqid')
const { AlignmentType, BorderStyle, Footer, HeightRule, VerticalAlign, Paragraph, Table, TableCell, TableRow, TextRun, WidthType, PageOrientation } = require("docx");
const docx = require("docx");
const fs = require("fs");
const path = require("path");
const dayjs = require("dayjs");
const weekday = require("dayjs/plugin/weekday");
const customParseFormat = require("dayjs/plugin/customParseFormat");

dayjs.extend(customParseFormat);
dayjs.extend(weekday);

const { docInfo, noBorders } = require("./reportWordHelpers");

module.exports = function (app) {
    app.post('/form', async (req, res) => {
        try {
            let data = req.fields;
            console.log('received form', data, typeof data)

            const documentPath = await generateDocWithData(data);

            let documentUrl = `/${documentPath}`;
            documentUrl = documentUrl.replace(/\\/g, '/');
            documentUrl = documentUrl.replace('/public', '');
            console.log('documentUrl', documentUrl)
            res.status(200).send({ status: 'success', data: { generatedDocumentUrl: documentUrl } });
        } catch (e) {
            res.status(500).send({ status: 'error', message: e.message });
        }
    })
}

async function generateDocWithData(data) {
    let randomId = uniqid();

    const tableData = [];
    tableData.push({
        header: 'ФИО',
        dataString: data.fio
    })
    tableData.push({
        header: 'Должность',
        dataString: data.position
    })
    tableData.push({
        header: 'Email',
        dataString: data.email
    })
    tableData.push({
        header: 'Телефон',
        dataString: data.phone
    })

    // `public/generated_docs/${randomId}`
    const docPath = await makeWord('public/generated_docs', 'Сгенерированный_документ.docx', { employmentDate: data.employmentDate, tableData })

    return docPath;
}

async function makeWord(directoryPath, fileName, documentData) {
    // console.log("in makeWord");

    let docData = [];

    let documentHeading1 = `Прошу трудоустроить меня на работу с ${documentData.employmentDate}`;
    let documentHeading2 = `Дата: ${dayjs().format('DD.MM.YYYY')}`;
    docData.push(
        new Paragraph({
            text: documentHeading1,
            style: "Heading1JustBold",
        })
    );
    docData.push(
        new Paragraph({
            text: documentHeading2,
            style: "Heading1JustBold",
        })
    );
    // Пустая строка
    docData.push(
        new Paragraph({
            text: '',
            style: "Heading1JustBold",
        })
    );

    let reportTableData = await makeDocTable({
        reportData: documentData,
    });

    docData.push(reportTableData);
    // console.log("docData", docData);

    let docInfoData = docInfo('Сгенерированный документ', {
        sections: [
            {
                properties: {},
                children: docData,
            },
        ],
    });
    // console.log("docInfoData", docInfoData);

    const doc = new docx.Document(docInfoData);
    // console.log("doc created");

    let docBuff = await docx.Packer.toBuffer(doc);
    const docPath = path.join(directoryPath, fileName)
    console.log('docPath', docPath)
    await fs.promises.mkdir(directoryPath, { recursive: true })
    await fs.promises.writeFile(docPath, docBuff);

    return docPath;

    async function makeDocTable({ reportData }) {
        let rows = [];

        //   console.log("makeReportTable", reportData);

        for (let row of reportData.tableData) {
            let rowData = new TableRow({
                height: {
                    value: 400,
                    rule: HeightRule.ATLEAST,
                },
                children: [
                    new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                            size: 50,
                            type: WidthType.PERCENTAGE,
                        },
                        margins: { left: 50, right: 50, top: 50, bottom: 50, marginUnitType: 'dxa' },
                        children: [
                            new Paragraph({
                                text: `${row.header}`,
                                style: "TableCellHeader",
                            }),
                        ],
                    }),
                    new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                            size: 50,
                            type: WidthType.PERCENTAGE,
                        },
                        margins: { left: 50, right: 50, top: 50, bottom: 50, marginUnitType: 'dxa' },
                        children: [
                            new Paragraph({
                                text: `${row.dataString}          `,
                                style: "TableCellDefault",
                            }),
                        ],
                    }),
                ],
            });
            rows.push(rowData);
        }

        // а теперь сама таблица
        let reportTable = new Table({
            alignment: AlignmentType.CENTER,
            width: {
                size: 100,
                type: WidthType.PERCENTAGE,
            },
            rows: [
                ...rows,
            ],
        });

        return reportTable;
    }
}